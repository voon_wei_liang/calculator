//
//  ViewController.m
//  calculator
//
//  Created by Hoi on 9/7/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    numbersArray=[[NSMutableArray alloc]init]; //store numbers for calculation
    operatorsArray=[[NSMutableArray alloc]init]; //store operators for calculation
    equationString=[[NSMutableString alloc]init]; //store whole equation to be stored in the arrays
    numberString=[[NSMutableString alloc]init]; // store the numbers temporarily before adding to the number array
    debugnumberarray=[[NSMutableString alloc]init]; //show the content of number array for debug purpose
    debugoperatorsarray=[[NSMutableString alloc]init]; //show the content of operator array for debug purpose
    [equationString setString:@""]; //prevent potential rubbish in equationstring
}



//clear arrays
//reset strings
//reset the label
- (IBAction)clearButton:(id)sender {
    [equationString setString:@""];
    [numberString setString:@""];
    [numbersArray removeAllObjects];
    [operatorsArray removeAllObjects];
    
    self.equationView.text=@"Equation";
    self.answerView.text=@"Answer";
}

//calculate function
- (IBAction)calculateButton:(id)sender {
    NSString *immutableString = [NSString stringWithString:equationString]; //convert string so that string manipulation can be used
    int multiplicationOrDivision=0; //store the number of 'x' or '/'
    hasError=false;
    

    if (!hasError) {
        
        
        //check each character of immutableString, if operator is detected:
        //  push the character to the operatorsArray
        //  push the numberString to numbersArray
        //  reset numberString to be ready to store new number
        //else
        //  append the character to numString
        //if the cursor reached the last number, push the numberString to numbersArray
        for (int x=0; x<[immutableString length];x++) {
            if ([immutableString characterAtIndex:x]=='+'||[immutableString characterAtIndex:x]=='-'||[immutableString characterAtIndex:x]=='x'||[immutableString characterAtIndex:x]=='/') {
                [operatorsArray addObject:[NSString stringWithFormat:@"%c",[immutableString characterAtIndex:x]]];
                [numbersArray addObject:[NSString stringWithFormat:@"%@",numberString]]; //if no use stringwithformat then the value will copy the next value for example input 6 9 will become 9 9
                [numberString setString:@""];
            }else{
                [numberString appendString:[NSString stringWithFormat:@"%c",[immutableString characterAtIndex:x]]];
            }
            if ([immutableString length]-1==x) {
                [numbersArray addObject:[NSString stringWithFormat:@"%@",numberString]];
            }
        }
        
    }
    
    //detect number of 'x' or '/'
    for (int x=0; x<operatorsArray.count; x++) {
        if ([operatorsArray[x] isEqualToString:@"x"]) {
            multiplicationOrDivision+=1;
        }else if ([operatorsArray[x] isEqualToString:@"/"]) {
            multiplicationOrDivision+=1;
        }
    }
 
    if (!hasError) {
        //do while there is operator in the operator array
        //  if there is multiplication or division to be done
        //    calculate and put answer into the array of the first number, array of the used operator is -
        //    removed, the array of second number is removed
        //  if there is no more multiplication or division to be done
        //    calculate and put answer into the array of the first number, array of the used operator-
        //    is removed, the array of second number is removed
        //display the answer
        
        do{
            if(multiplicationOrDivision>0)
            {
                for (int x=0; x<operatorsArray.count; x++) {
                    if ([operatorsArray[x] isEqualToString:@"/"]) {
                        
                        numbersArray[x]=[NSString stringWithFormat:@"%f",[numbersArray[x] doubleValue]/[numbersArray[x+1] doubleValue]];
                        [operatorsArray removeObjectAtIndex:x];
                        [numbersArray removeObjectAtIndex:(x+1)];
                        multiplicationOrDivision-=1;
                        break;
                    }else if ([operatorsArray[x] isEqualToString:@"x"]) {
                        numbersArray[x]=[NSString stringWithFormat:@"%f",[numbersArray[x] doubleValue]*[numbersArray[x+1] doubleValue]];
                        [operatorsArray removeObjectAtIndex:x];
                        [numbersArray removeObjectAtIndex:x+1];
                        multiplicationOrDivision-=1;
                        break;
                    }
                }
            }
            if(multiplicationOrDivision==0){
                if ([operatorsArray[0] isEqualToString:@"+"]) {
                    numbersArray[0]=[NSString stringWithFormat:@"%f",[numbersArray[0] doubleValue]+[numbersArray[1] doubleValue]];
                    [operatorsArray removeObjectAtIndex:0];
                    [numbersArray removeObjectAtIndex:1];
                }else if ([operatorsArray[0] isEqualToString:@"-"]) {
                    numbersArray[0]=[NSString stringWithFormat:@"%f",[numbersArray[0] doubleValue]-[numbersArray[0+1] doubleValue]];
                    [operatorsArray removeObjectAtIndex:0];
                    [numbersArray removeObjectAtIndex:1];
                }
            }
        }while (operatorsArray.count>0);
    }
    self.answerView.text=numbersArray[0];
}

- (IBAction)zeroButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'0']];
    self.equationView.text=equationString;
}

- (IBAction)oneButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'1']];
    self.equationView.text=equationString;
}

- (IBAction)twoButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'2']];
    self.equationView.text=equationString;
}

- (IBAction)threeButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'3']];
    self.equationView.text=equationString;
}

- (IBAction)fourButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'4']];
    self.equationView.text=equationString;
}

- (IBAction)fiveButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'5']];
    self.equationView.text=equationString;
}

- (IBAction)sixButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'6']];
    self.equationView.text=equationString;
}

- (IBAction)sevenButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'7']];
    self.equationView.text=equationString;
}

- (IBAction)eightButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'8']];
    self.equationView.text=equationString;
}

- (IBAction)nineButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'9']];
    self.equationView.text=equationString;
}

- (IBAction)dotButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'.']];
    self.equationView.text=equationString;
}

- (IBAction)minusButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'-']];
    self.equationView.text=equationString;
}

- (IBAction)plusButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'+']];
    self.equationView.text=equationString;
}

- (IBAction)multiplyButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'x']];
    self.equationView.text=equationString;
}

- (IBAction)divideButton:(id)sender {
    [equationString appendString:[NSString stringWithFormat:@"%c",'/']];
    self.equationView.text=equationString;
}

//function to convert decimal answer to hexadecimal answer
- (IBAction)ConvertButton:(id)sender {
    int answer=0;
    NSMutableArray *remainder=[[NSMutableArray alloc]init];
    NSMutableString *finalAnswer=[[NSMutableString alloc]init];
    answer=[self.answerView.text intValue];
    
    do{
        //put remainder into the remainder array
        //store integer style division answer into answer
        [remainder insertObject:[NSString stringWithFormat:@"%i",answer%16] atIndex:0];
        answer=answer/16;
    }while (!(answer/16==0));
    
    //do extra because do while loop stopped too early
    [remainder insertObject:[NSString stringWithFormat:@"%i",answer%16] atIndex:0];
    answer=answer/16;
    
    //change into special character if needed and append to the final answer
    //or just append normal number to the final answer
    for (int x; x<remainder.count; x++) {
            NSLog(@"content %@",remainder[x]);
            switch ([remainder[x] intValue]) {
                case 10:
                    [finalAnswer appendString:@"A"];
                    break;
                case 11:
                    [finalAnswer appendString:@"B"];
                    break;
                case 12:
                    [finalAnswer appendString:@"C"];
                    break;
                case 13:
                    [finalAnswer appendString:@"D"];
                    break;
                case 14:
                    [finalAnswer appendString:@"E"];
                    break;
                case 15:
                    [finalAnswer appendString:@"F"];
                    break;
                
                default:
                    [finalAnswer appendString:[NSString stringWithFormat:@"%i",[remainder[x] intValue]]];
                    break;
            }
    }

    //display final answer
    self.answerView.text=finalAnswer;
}

@end









































