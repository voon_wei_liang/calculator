//
//  AppDelegate.h
//  calculator
//
//  Created by Hoi on 9/7/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

