//
//  ViewController.h
//  calculator
//
//  Created by Hoi on 9/7/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    NSMutableArray *numbersArray,*operatorsArray;
    NSMutableString *equationString,*numberString,*debugoperatorsarray,*debugnumberarray;
    Boolean isNumber,hasError;
}

@property (weak, nonatomic) IBOutlet UILabel *equationView;
@property (weak, nonatomic) IBOutlet UILabel *answerView;

- (IBAction)zeroButton:(id)sender;
- (IBAction)oneButton:(id)sender;
- (IBAction)twoButton:(id)sender;
- (IBAction)threeButton:(id)sender;
- (IBAction)fourButton:(id)sender;
- (IBAction)fiveButton:(id)sender;
- (IBAction)sixButton:(id)sender;
- (IBAction)sevenButton:(id)sender;
- (IBAction)eightButton:(id)sender;
- (IBAction)nineButton:(id)sender;
- (IBAction)dotButton:(id)sender;
- (IBAction)clearButton:(id)sender;
- (IBAction)calculateButton:(id)sender;
- (IBAction)plusButton:(id)sender;
- (IBAction)multiplyButton:(id)sender;
- (IBAction)minusButton:(id)sender;
- (IBAction)divideButton:(id)sender;
- (IBAction)ConvertButton:(id)sender;

@end

